const mysql = require('mysql');

exports.db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'radius'
});

exports.queryraw = 'SELECT * FROM users WHERE email = ?';
