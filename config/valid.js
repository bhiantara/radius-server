var express = require('express');
var jwt = require("jsonwebtoken");
var validator = express.Router();

validator.route('/')
    .get(valid);

function valid(req,res,next){
    var token = req.headers['token'];
    if(token){
        jwt.verify(token, key, function(err){
            if(err){
                return res.send({error: true, message: 'Anda harus login kembali'}); // token invalid or token expired
            }else{
                next(); // valid token
            }
        })
    }else if (!token){
        return res.send({error: true, message: 'Anda harus login'}); // token null
    }else{
        return res.send({error: true, message: 'unknown eror'}); // unknown eror
    }
}
module.exports = validator;